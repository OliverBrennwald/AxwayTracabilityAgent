# AxwayTracabilityAgent



## Getting started
This Zip File was made because Axway does not provide a downloadable version of the Tracability Agent by themselve.

Source:
https://docs.axway.com/bundle/amplify-central/page/docs/connect_manage_environ/connected_agent_common_reference/traceability_usage/index.html#set-up-usage-reporting-in-offline-mode


The axway.zip File contains the axway npm package with all dependancies and is made with:

`npm install -g axway`

The tracability-agent.zip contains the actuall agent, as a runnable configuration.
